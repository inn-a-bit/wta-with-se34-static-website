package wta.selenium.run;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class WTA {
	private static WebDriver driver;
	private static WebDriverWait wait;
	private static ChromeOptions options;
	SimpleActionsPage sap = new SimpleActionsPage();
	SummaryActionsPage sum_ap = new SummaryActionsPage();
	String parentWindow = "";

	@BeforeClass
	public static void beforeClassTest() {
		String baseURL = "https://wta-with-se34-webdriver.info";
		System.setProperty("webdriver.chrome.driver", "/path/to/your/Selenium/chromedriver");
		options = new ChromeOptions();
		options.addArguments("--start-maximazed");
		driver = new ChromeDriver(options);
		driver.get(baseURL);
		wait = new WebDriverWait(driver, 10);
	}

	@AfterClass
	public static void afterClassTest() throws Exception {
		driver.close();
	}

	@Test
	public void test_a_a_fnu_resume_summary() throws Exception {
		sap.clickLink(driver, wait, "/html/body/div[1]/table/tbody/tr[3]/td/a");
		parentWindow = sap.switchToNewWindow(driver);
		sap.switchToIFrame(driver, "iFrame3");
		sap.clickLink(driver, wait, "//*[@id=\"skills\"]/tbody/tr[2]/td[1]/a");
		String parentWindow2 = sap.switchToNewWindow(driver);
		Point point = new Point(200, 100);
		driver.manage().window().setPosition(point);
		Dimension d = new Dimension(700, 1100);
		driver.manage().window().setSize(d);
		sap.switchToParentWindow(driver, parentWindow2);
	}

	@Test
	public void test_a_checkbox_summary() throws Exception {
		sap.switchToIFrame(driver, "iFrame4");
		sap.scrollByPixel(driver);
		WebElement element = sap.clickCheckbox(driver, wait, "//*[@id=\"skills\"]/tbody/tr[2]/td[1]/input");
		AssertJUnit.assertEquals("true",element.getAttribute("checked"));
	}

	@Test
	public void test_b_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "15", "//*[@id=\"skills\"]/tbody/tr[2]/td[3]/input");
		AssertJUnit.assertTrue(text.equals("15"));
	}

	@Test
	public void test_c_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "2020", "//*[@id=\"skills\"]/tbody/tr[2]/td[4]/input");
		AssertJUnit.assertTrue(text.equals("2020"));
	}

	@Test
	public void test_d_dropDownByXPath() throws Exception {
		String value = sap.selectFromDropDownByXpath(driver, wait, 2, "//*[@id=\"skills\"]/tbody/tr[2]/td[5]/select");
		AssertJUnit.assertTrue(value.equals("Advanced"));
	}

	@Test
	public void test_e_radioButton() throws Exception {
		String value = sap.selectRadioButton(driver, wait,
				"/html/body/div[1]/form/table[2]/tbody/tr[2]/td[6]/input[2]");
		AssertJUnit.assertTrue(value.equals("Update"));
	}

	@Test
	public void test_f_checkbox_summary() throws Exception {
		WebElement element = sap.clickCheckbox(driver, wait, "//*[@id=\"skills\"]/tbody/tr[4]/td[1]/input");
		AssertJUnit.assertEquals("true",element.getAttribute("checked"));
	}

	@Test
	public void test_g_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "1", "//*[@id=\"skills\"]/tbody/tr[4]/td[3]/input");
		AssertJUnit.assertTrue(text.equals("1"));
	}

	@Test
	public void test_h_text_summary() throws Exception {
		String text = sum_ap.setText(driver, wait, "2020", "//*[@id=\"skills\"]/tbody/tr[4]/td[4]/input");
		AssertJUnit.assertTrue(text.equals("2020"));
	}

	@Test
	public void test_i_dropDownByXPath() throws Exception {
		String value = sap.selectFromDropDownByXpath(driver, wait, 4, "//*[@id=\"skills\"]/tbody/tr[4]/td[5]/select");
		AssertJUnit.assertTrue(value.equals("Novice"));
	}

	@Test
	public void test_j_radioButton() throws Exception {
		String value = sap.selectRadioButton(driver, wait,
				"/html/body/div[1]/form/table[2]/tbody/tr[4]/td[6]/input[2]");
		AssertJUnit.assertTrue(value.equals("Update"));
	}
	
	@Test
	public void test_ja_findElementInTable() throws Exception {
		sap.switchToParentWindow(driver);
		sap.switchToIFrame(driver, "iFrame5");
		boolean value = sum_ap.findElementInTable(driver, wait, "Selenium");
		AssertJUnit.assertEquals(true, value);
	}

	@Test
	public void test_k_click_checkbox() throws Exception {
		sap.switchToParentWindow(driver);
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 2);
		parentWindow = sap.switchToNewWindow(driver);
		WebElement element = sap.clickCheckbox(driver, wait, "/html/body/div[1]/div[2]/table/tbody/tr[2]/td/label/span");
		AssertJUnit.assertNotNull(element);	
	}

	@Test
	public void test_l_click_checkbox_by_index() throws Exception {
		WebElement element = sap.clickCheckboxByIndex(driver, wait, 2);
		AssertJUnit.assertNotNull(element);
	}

	@Test
	public void test_m_uncheck_checkbox() throws Exception {
		WebElement element = sap.uncheckCheckBox(driver, wait, "/html/body/div[1]/div[2]/table/tbody/tr[2]/td/label/span");
		AssertJUnit.assertEquals(null, element);
	}

	@Test
	public void test_n_uncheck_checkbox_by_index() throws Exception {
		WebElement element = sap.uncheckCheckBoxByIndex(driver, wait, 2);
		AssertJUnit.assertEquals(false,element.isSelected());	
	}

	@Test
	public void test_o_click_radio_button() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 3);
		parentWindow = sap.switchToNewWindow(driver);
		sap.selectRadioButton(driver, wait, 2);
		sap.selectRadioButton(driver, wait, 1);
	}

	@Test
	public void test_p_populate_textbox() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 4);
		parentWindow = sap.switchToNewWindow(driver);
		sap.setText(driver, wait, "Елена", "first_name_2");
		sap.setText(driver, wait, "Прекрасная", "last_name");
		sap.setText(driver, wait, "test@yahoo.com", "email_id");
	}

	@Test
	public void test_r_select_from_dropdown() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 5);
		parentWindow = sap.switchToNewWindow(driver);
		String value = sap.selectFromDropDown(driver, wait, 2, "skill-level");
		AssertJUnit.assertEquals("Advanced", value);
	}

	@Test
	public void test_s_click_submit() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 6);
		parentWindow = sap.switchToNewWindow(driver);
		String value = sap.clickSubmitWithAlert(driver, wait, "//*[@id=\"submit_it\"]");
		AssertJUnit.assertEquals("Don't forget to test alert!!", value);
	}

	@Test
	public void test_t_findElementInTable() throws Exception {

		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 7);
		parentWindow = sap.switchToNewWindow(driver);
		sap.switchToIFrame(driver, "iFrame5");
		boolean value = sum_ap.findElementInTable(driver, wait, "Selenium");
		AssertJUnit.assertEquals(true, value);
	}

	@Test
	public void test_tt_click_link() throws Exception {
		sap.switchToParentWindow(driver, parentWindow);
		sap.clickLink(driver, wait, 8);
		parentWindow = sap.switchToNewWindow(driver);
		sap.clickLink(driver, wait, "/html/body/div[2]/h3[2]/a");
		String parentWindow2 = sap.switchToNewWindow(driver);
		Point point = new Point(200, 100);
		driver.manage().window().setPosition(point);
		Dimension d = new Dimension(700, 1100);
		driver.manage().window().setSize(d);
		sap.switchToParentWindow(driver, parentWindow2);
		sap.switchToParentWindow(driver, parentWindow);
	}
	
	private class SimpleActionsPage {
		private WebElement clickCheckbox(WebDriver driver, WebDriverWait wait, String xpath) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			Actions actions = new Actions(driver);
			WebElement element = null;
			if (!driver.findElement(By.xpath(xpath)).isSelected()) {
				element = driver.findElement(By.xpath(xpath));
				actions.moveToElement(element).click().build().perform();
			}
			return element;
		}

		private WebElement uncheckCheckBox(WebDriver driver, WebDriverWait wait, String xpath) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			Actions actions = new Actions(driver);
			WebElement element = null;
			if (driver.findElement(By.xpath(xpath)).isSelected()) {
				element = driver.findElement(By.xpath(xpath));
				actions.moveToElement(element).click().build().perform();
			}
			return element;
		}

		private WebElement clickCheckboxByIndex(WebDriver driver, WebDriverWait wait, int index) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span")));
			Actions actions = new Actions(driver);
			WebElement element = null;
			if (!driver.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"))
					.isSelected()) {
				element = driver
						.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"));
				actions.moveToElement(element).click().build().perform();
			}
			return element;
		}
		
		private WebElement uncheckCheckBoxByIndex(WebDriver driver, WebDriverWait wait, int index) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(
					By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span")));
			Actions actions = new Actions(driver);
			WebElement element = null;
			element = driver
					.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[" + index + "]/td/label/span"));
			actions.moveToElement(element).click().build().perform();
			return element;
		}

		private void clickLink(WebDriver driver, WebDriverWait wait, int index) throws Exception {
			wait.until(ExpectedConditions
					.visibilityOfElementLocated(By.xpath("//*[@id=\"exploring\"]/ul/li[" + index + "]/a")));
			WebElement element = null;
			element = driver.findElement(By.xpath("//*[@id=\"exploring\"]/ul/li[" + index + "]/a"));
			scrollIntoView(driver, element);
			element.click();
		}

		private void clickLink(WebDriver driver, WebDriverWait wait, String xpath) 
		{ 
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))); 
			WebElement element = driver.findElement(By.xpath(xpath));	
			element.click(); 
		}
		private void scrollByPixel(WebDriver driver) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		}

		private void scrollIntoView(WebDriver driver, WebElement element) {
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].scrollIntoView();", element);
		}

		private void switchToIFrame(WebDriver driver, String iframeId) {
			WebElement iframeSwitch = driver.findElement(By.id(iframeId));
			driver.switchTo().frame(iframeSwitch);
		}

		private void switchToParentWindow(WebDriver driver) {
			driver.switchTo().defaultContent();
		}

		private String switchToNewWindow(WebDriver driver) {
			String parentWind = driver.getWindowHandle();
			for (String childWin : driver.getWindowHandles()) {
				driver.switchTo().window(childWin);
			}

			return parentWind;
		}

		private void switchToParentWindow(WebDriver driver, String parentWindow) {
			driver.close();
			driver.switchTo().window(parentWindow);
		}

		private void selectRadioButton(WebDriver driver, WebDriverWait wait, int index) 
		{ 
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath( "/html/body/div/div[2]/table/tbody/tr[" + index + "]/td/label/span")))); 
			WebElement radioBtn = driver.findElement(By.xpath( "/html/body/div/div[2]/table/tbody/tr[" + index + "]/td/label/span")); 
			radioBtn.click(); 
		}
		private String selectRadioButton(WebDriver driver, WebDriverWait wait, String xpath) {
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(xpath))));
			WebElement radioBtn = driver.findElement(By.xpath(xpath));
			radioBtn.click();
			return radioBtn.getAttribute("value");
		}

		private String selectFromDropDown(WebDriver driver, WebDriverWait wait, int index, String id_str) 
		{ 
			Select dropDown = new Select(driver.findElement(By.id(id_str))); 
			dropDown.selectByIndex(index); 
			return dropDown.getAllSelectedOptions().get(0).getText();	
		}
		private String selectFromDropDownByXpath(WebDriver driver, WebDriverWait wait, int index, String xpath) {
			Select dropDown = new Select(driver.findElement(By.xpath(xpath)));
			dropDown.selectByIndex(index);
			return dropDown.getAllSelectedOptions().get(0).getText();
		}

		private String clickSubmitWithAlert(WebDriver driver, WebDriverWait wait, String xpath) 
		{ 
			WebElement webel = driver.findElement(By.xpath(xpath)); 
			JavascriptExecutor executor = (JavascriptExecutor)driver; 
			executor.executeScript("arguments[0].click();", webel); 
			wait.until(ExpectedConditions.alertIsPresent()); 
			String alertText = driver.switchTo().alert().getText(); 
			driver.switchTo().alert().accept(); 
			return alertText; 
		}
		private String setText(WebDriver driver, WebDriverWait wait, String text, String element) 
		{ 
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(element))); 
			driver.findElement(By.id(element)).sendKeys(text); 
			return driver.findElement(By.id(element)).getAttribute("value"); 
		}
	}

	private class SummaryActionsPage {

		private boolean findElementInTable(WebDriver driver, WebDriverWait wait, String text) {
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"skills\"]/tbody")));
			SimpleActionsPage sap = new SimpleActionsPage();
			WebElement table = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody"));
			List<WebElement> rowsCltn = table.findElements(By.xpath("//*[@id=\"skills\"]/tbody/tr"));
			WebElement element = null;
			if (rowsCltn.size() > 0) {
				String cell_text = "";
				for (int i = 2; i <= rowsCltn.size(); i++) {
					element = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody/tr[" + i + "]/td[1]"));
					cell_text = driver.findElement(By.xpath("//*[@id=\"skills\"]/tbody/tr[" + i + "]/td[1]")).getText();
					if (cell_text.contains(text)) {
						sap.scrollIntoView(driver, element);
						((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", element);
						return true;
					}
				}
			}
			return false;
		}

		private String setText(WebDriver driver, WebDriverWait wait, String text, String xpath) {

			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
			WebElement element = driver.findElement(By.xpath(xpath));
			element.sendKeys(text);
			String check_text = driver.findElement(By.xpath(xpath)).getAttribute("value");
			return check_text;
		}
	}
}
