$(document).ready(function(){

    $('input:checkbox[name=skills-checkbox]').on('change', function() {
        if (!this.checked) {
            $(this).parent().next().next().find('input.text-box[type=text]').val('');
            $(this).parent().next().next().next().find('input.text-box2[type=text]').val('');
            $(this).parent().next().next().next().next().find('input,select').find('option:first').attr('selected', 'selected');
            $(this).parent().next().next().next().next().next().find('input[type=radio]').filter("[value='Add']").prop("checked",true);
        }
    });
    $("#skills-responses").hide();
    $("form#skills-inventor").submit(function(event){
        event.preventDefault();
        var selectedArray = new Array();
        //$("#skills-responses").show();
        var  recCount = 0;
        var myJsonString;
        $("input:checkbox[name=skills-checkbox]:checked").each(function() {
            selectedArray[recCount] = {
            "skill" : $(this).parent().next().text(),
            "howManyYears" : $(this).parent().next().next().find('input.text-box[type=text]').val(),
            "lastTimeUsed" : $(this).parent().next().next().next().find('input.text-box2[type=text]').val(),
            "skillLevel" : $(this).parent().next().next().next().next().find('input,select').val(),
            "actionToPerform" : $(this).parent().next().next().next().next().next().find('input[type=radio]:checked').val()
            }
            recCount++;
        });
        myJsonString = JSON.stringify(selectedArray);
        alert(myJsonString);
    });
    $(document).on("change","#skills tbody input[type='checkbox']", function(){  	
    	$(this).closest("tr").find("input").not(this).prop("disabled", !this.checked)
    	$(this).closest("tr").find("select[name=select-skill]").prop("disabled", !this.checked);
    	if($('[name="skills-checkbox"]:checked').length == 0)
    	{
    		$('#submi').prop("disabled",true);
    	}
    	else
    	{
    		$('#submi').prop("disabled",false);
    	}
    });
});